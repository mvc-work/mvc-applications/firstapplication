﻿using System;
using System.Web.Mvc;

namespace FirstApp.Controllers
{
    public class ViewToControlDataTransferController : Controller
    {
        //
        // GET: /Department/

        public ActionResult Index()
        {
            return View();
        }

        public string GetMessage()
        {
            return "Hello Pramod Karale inside GetMessage";
        }

        public string GetDateTime()
        {
            return "Hello Pramod Karale inside GetDateTime: " + DateTime.Now.ToString();
        }

        #region Passing values from View to Controller

        //only work for GET Method:- QueryString Method
        public string PostDataQueryString()
        {
            string name = Request.QueryString["FName"].ToString();

            return "Hello Pramod Karale inside PostData Name=" + name;
        }

        //GET/POST Method: - Parameterized Method
        public string PostDataParameter(string FName)
        {
            return "Hello Pramod Karale inside PostData Name=" + FName;
        }

        //only work for POST Method:- Context Object/FormCollection Method
        public string PostDataContextObject(FormCollection FC)
        {
            string name = FC["FName"].ToString();

            return "Hello Pramod Karale inside PostData Name=" + name;
        }

        #endregion
    }
}
