namespace FirstApp.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Dept_ModelEntityFrameworkDemo : DbContext
    {
        public Dept_ModelEntityFrameworkDemo()
            : base("name=Dept_ModelEntityFrameworkDemo")
        {
        }

        public virtual DbSet<Department> Departments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .Property(e => e.HODName)
                .IsUnicode(false);
        }
    }
}
