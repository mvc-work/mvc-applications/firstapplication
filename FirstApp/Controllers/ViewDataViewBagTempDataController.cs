﻿using System.Web.Mvc;

namespace FirstApp.Controllers
{
    // in this controller we are implementing View_Data, View_Bag, Temp_Data and we are using Html helper classes.i.e controller to view data transfer.
    public class ViewDataViewBagTempDataController : Controller
    {
        //
        // GET: /ViewDataViewBagTempData/

        public ActionResult Index()
        {
            string Name = string.Empty;
            //View Bag and view data will not be available here

            string ViewBagName = string.Empty;

            ViewBagName = ViewBag.FName + " " + ViewBag.LName;

            if (ViewData["FName"] != null && ViewData["LName"] != null)
            {
                Name = ViewData["FName"].ToString() + " " + ViewData["LName"].ToString();
            }

            if (TempData["FName"] != null && TempData["LName"] != null)
            {
                Name = TempData["FName"].ToString() + " " + TempData["LName"].ToString();
            }

            return View();
        }

        //only work for POST Method:- Context Object/FormCollection Method
        public ActionResult PostDataContextObject(FormCollection FC)
        {
            ViewData["FName"] = FC["FName"].ToString();
            ViewData["LName"] = FC["LName"].ToString();

            TempData["FName"] = FC["FName"].ToString();
            TempData["LName"] = FC["LName"].ToString();

            ViewBag.FName = FC["FName"].ToString();
            ViewBag.LName = FC["LName"].ToString();

            //return View("Index");//this is to test view data and view bag
            return RedirectToAction("Index");//this is to test temp datadk
        }
    }
}