﻿using FirstApp.Models;
using System.Web.Mvc;

namespace FirstApp.Controllers
{
    public class ModelDemoController : Controller
    {
        //
        // GET: /ModelDemo/

        public ActionResult Index()
        {
            Dept_ModelDemo obj = new Dept_ModelDemo() { DName = "Computer Science", DHOD = "Shinde" };
            return View("ModelDemo",obj);
        }


        public ActionResult Insert(Dept_ModelDemo obj)
        {
            //we can pick the values from the above object and can insert data using ado .net
            return RedirectToAction("Index");
        }
    }
}
